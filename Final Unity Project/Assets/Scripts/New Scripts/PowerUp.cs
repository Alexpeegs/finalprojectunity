﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {

	public float addedHealth = 1.4f;
	public GameObject pickupEffect;

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			Pickup (other);
		}
	}
	void Pickup(Collider player)
	{
		Instantiate (pickupEffect, transform.position, transform.rotation);


		PlayerHealth stats = player.GetComponent<PlayerHealth> ();
		PlayerHealth slide = player.GetComponent<PlayerHealth> ();
		stats.currentHealth *= addedHealth;

		if (stats.currentHealth > 100)
		{
			stats.currentHealth = 100;
		}
		slide.healthSlider.value = stats.currentHealth;

		Destroy (gameObject);








	}
}
