﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTrap : MonoBehaviour {

	//NextLevel nextLevel;

	public GameObject GO;
    public GameObject pickupEffect;


    // Use this for initialization

//	void Awake()
//	{
//		nextLevel = GetComponent<NextLevel> ();
//	}

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.gameObject.tag == "Robot")
        {

			//nextLevel.SetActive(true);
			GO.SetActive(true);
            Instantiate(pickupEffect, transform.position, transform.rotation);
            Destroy(gameObject, .5f);


        }
        if (otherObj.gameObject.tag == "Player")
        {

            //nextLevel.SetActive(true);
            GO.SetActive(true);
            Destroy(gameObject, .5f);


        }
    }

}
