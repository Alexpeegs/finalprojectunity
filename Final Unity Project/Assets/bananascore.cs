﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bananascore : MonoBehaviour {
    
    public int scoreValue = 10;
    public GameObject pickupEffect;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision otherObj)
    {
        if (otherObj.gameObject.tag == "Robot")
        {

            //nextLevel.SetActive(true);
            Destroy(gameObject, .5f);
            ScoreManager.score += scoreValue;

        }
        if (otherObj.gameObject.tag == "Player")
        {
            Instantiate(pickupEffect, transform.position, transform.rotation);
            //nextLevel.SetActive(true);
            Destroy(gameObject, .5f);
            ScoreManager.score += scoreValue;

        }
    }
}
